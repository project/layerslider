/**
* @file
* Javascript, modifications of DOM.
*
* Manipulates links to include scrollreveal data
*/

(function ($) {
  Drupal.behaviors.layer_slider = {
    attach: function (context, settings) {
      jQuery("#layerslider").layerSlider({
        pauseOnHover: false,
        skinsPath: '/sites/all/libraries/layerslider/skins/'
      });
    }
  }  

}(jQuery));
