<?php

/**
 * @file
 * A basic template for layer slides
 *
 * Available variables:
 * - $items: An array of comment items. Use render($content) to print them all
 *
 * @see template_preprocess()
 */
?>

<div id="slider-wrapper">
<!-- LayerSlider start -->
  <div id="layerslider">
    <?php foreach ($items as $delta => $item): ?>
      <?php print render($item); ?>
    <?php endforeach; ?>
  </div>
</div>
