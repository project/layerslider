<?php

/**
 * @file
 * tpl file for theming a single slide
 *
 * Available variables:
 * - $title: Icon identifyg sucess
 * - $teaser: Time the sucess happenned
 * - $image: Comment for the sucess
 * - $link: Comment for the sucess
 * - $link_text: Comment for the sucess
 * - $background: Comment for the sucess
 * - $data_ls: Slide efect
 * - $data_ls_title: Slide efect
 * - $data_ls_teaser: Slide efect
 * - $data_ls_image: Slide efect
 * - $data_ls_link: Slide efect
 * - $number: slide number
 * 
 */
?>

<div id="ls-slide-<?php print $number ?>" class="ls-slide" data-ls="<?php print $data_ls ?>">
<!-- slide background image -->
  <img src="<?php print $background ?>" class="ls-bg" alt="Slide background"/>
<!-- layer one -->
  <h2 class="ls-l" data-ls="<?php print $data_ls_title ?>"><?php print $title ?></h1>
<!-- layer two -->
  <img class="ls-l" src="<?php print $image ?>" alt="Image layer" data-ls="<?php print $data_ls_image ?>">
<!-- layer three -->
  <div class="ls-l" data-ls="<?php print $data_ls_teaser ?>"><?php print $teaser ?></div>
<!-- layer four -->
  <a class="ls-l" href="<?php print $link ?>" data-ls="<?php print $data_ls_link ?>"><?php print $link_text ?></a>
</div>
